package uz.dilshodjon216.spotifyclone

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SpotifyApplication:Application()